env._G = GLOBAL
env.require = GLOBAL.require
env.io = GLOBAL.io
env.MODS_ROOT = GLOBAL.MODS_ROOT

local mod_data = GetModConfigData("mods")
local whitelist = GetModConfigData("whitelist")
local VALID = {}

if type(mod_data) == "table" then
	VALID = mod_data
else
	VALID = {
		["workshop-343753877"] = true,
		["workshop-351325790"] = true,
		["workshop-352373173"] = true,
		["workshop-365119238"] = true,
		["workshop-367546858"] = true,
		["workshop-433644012"] = true,
		["workshop-661284815"] = true,
		["workshop-727774324"] = true,
		["workshop-1240565842"] = true,
		["workshop-1594406980"] = true,
	}
end

VALID["workshop-1813806139"] = true

local _cpt = _G.KnownModIndex.IsModCompatibleWithMode
_G.KnownModIndex.IsModCompatibleWithMode = function(self, modname, ...)
	if _G.rawget(_G, "TheNet") and type(whitelist) == "table" and whitelist[_G.TheNet:GetUserID()] then
		return _cpt(self, modname, ...)
	end
	local modinfo = _G.KnownModIndex:GetModInfo(modname)
	if not VALID[modname] and (not modinfo or modinfo.client_only_mod) then
		return false
	end
	return _cpt(self, modname, ...)
end

if _G.rawget(_G, "ModManager") then
	local ModManager = _G.ModManager
	local package = _G.package
	local _InitializeModMain = ModManager.InitializeModMain
	ModManager.InitializeModMain = function(self, modname, ...)
		if _G.rawget(_G, "TheNet") and type(whitelist) == "table" and whitelist[_G.TheNet:GetUserID()] then
			return _InitializeModMain(self, modname, ...)
		end
		
		local modinfo = _G.KnownModIndex:GetModInfo(modname)
		if not VALID[modname] and (not modinfo or modinfo.client_only_mod) then
			local ppath = string.split(package.path, ";")
			for i, path in ipairs(ppath) do
				local id = path:match("[workshop-]+%d+")
				if id then
					local modinfo = _G.KnownModIndex:GetModInfo(id)
					if not VALID[id] and (not modinfo or modinfo.client_only_mod) then
						table.remove(ppath, i)
					end
				end
			end
			package.path = table.concat(ppath, ";")
		end
		
		return _InitializeModMain(self, modname, ...)
	end
end

do
	local f = io.open(MODS_ROOT.."modsettings.lua", "w")
	if not f then
		print("[ACE] Warning! modsettings.lua is missing.")
		return
	end
	
	local dn = "workshop-1813806139"
	local old = {}
	for line in io.lines(MODS_ROOT.."modsettings.lua") do 
		table.insert(old, line)
		local str = line:gsub(" ", ""):gsub("ForceEnableMod(", ""):gsub("-", "")
		if str:match("[%w_]+") == dn:gsub("-", "") then
			print("[ACE] Already tracking the client side.")
			return
		end
	end
	
	print("[ACE] Enabling mod...")
	for i, str in ipairs(old) do
		if str ~= "ForceEnableMod(\""..dn.."\")\n" and not str:find("1663300520") then
			f:write(str.."\n")
		end
	end
	f:write("ForceEnableMod(\""..dn.."\")")
	f:close()
	print("[ACE] Done.")
end

