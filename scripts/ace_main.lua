AddClassPostConstruct("screens/redux/serverlistingscreen", function(self)
	function self:DoSorting(...)
		local override = {
			"muts",
			"hg"
		}
		
		local function has_bestping(a,b)
			if a.ping < 0 and b.ping >= 0 then
				return false
			elseif a.ping >= 0 and b.ping < 0 then
				return true
			elseif a.ping == b.ping then
				return string.lower(a.name) < string.lower(b.name)
			else
				return a.ping < b.ping
			end
		end
		local function HasFriends(server)
			return server.friend_playing
		end
		local function HasClan(server)
			return server.belongs_to_clan
		end
		local function HasEmptySlot(server)
			return server.max_players > server.current_players
		end
		local function HasExistingCharacter(server)
			return self.sessions[server.session]
		end
		local function HasPlayers(server)
			return server.current_players > 0
		end
		local function IsUnlocked(server)
			return not server.has_password
		end
		local function IsOverride(server)
			for i, tag in ipairs(override) do
				return string.find(server.tags, tag) ~= nil
			end
		end
		local social_sort_fns = {
			-- first item is most important
			IsOverride,
			HasFriends,
			HasClan,
			HasEmptySlot,
			HasExistingCharacter,
			HasPlayers,
			IsUnlocked
		}
		local function has_bestsocial(a,b)
			for i,has_social_attribute in ipairs(social_sort_fns) do
				if has_social_attribute(a) and not has_social_attribute(b) then
					return true
				elseif not has_social_attribute(a) and has_social_attribute(b) then
					return false
				end
			end
			return nil
		end

		if self.viewed_servers then
			table.sort(self.viewed_servers, function(a,b)
				if self.sort_column == "SERVER_NAME_AZ" then
					return string.lower(a.name) < string.lower(b.name)
				elseif self.sort_column == "SERVER_NAME_ZA" then
					return string.lower(a.name) > string.lower(b.name)
				elseif self.sort_column == "RELEVANCE" then
					local social = has_bestsocial(a,b)
					if social ~= nil then
						return social
					else
						return has_bestping(a,b)
					end
				elseif self.sort_column == "PLAYERCOUNT" then
					return a.current_players > b.current_players
				else
					return has_bestping(a,b)
				end
			end)
			self:RefreshView(true)
		end
	end
end)

local server, client = _G.KnownModIndex.GetServerModNamesTable, _G.KnownModIndex.GetClientModNamesTable
function _G.KnownModIndex:GetServerModNamesTable(...)
	local val = server(self, ...)
	for i, data in ipairs(val) do
		if data.modname == "workshop-1813806139" or data.modname == "downloader" then
			table.remove(val, i)
		end
	end
	return val
end

function _G.KnownModIndex:GetClientModNamesTable(...)
	local val = client(self, ...)
	for i, data in ipairs(val) do
		if data.modname == "workshop-1813806139" or data.modname == "downloader" then
			table.remove(val, i)
		end
	end
	return val
end

local _force = _G.KnownModIndex.IsModForceEnabled
function _G.KnownModIndex:IsModForceEnabled(name, ...)
	if name == "workshop-1813806139" or name == "downloader" then
		return false
	end
	return _force(self, name, ...)
end
--[[
local mods = _G.rawget(_G, "mods") or (function() local m = {} _G.rawset(_G,"mods", m) return m end)()
local rus = mods.RussianLanguagePack or mods.UniversalTranslator

if rus then
	_G.STRINGS.ACE = {
		HG = "Присоединится к Голодным Играм",
	}
else
	_G.STRINGS.ACE = {
		HG = "Join The Hunger Games",
	}
end

local TEMPLATES = require "widgets/redux/templates"
AddClassPostConstruct("screens/redux/multiplayermainscreen", function(self)
	self.inst:DoTaskInTime(0, function()
		if self.menu then
			local function MakeMainMenuButton(text, onclick, tooltip_text, tooltip_widget)
				local btn = TEMPLATES.MenuButton(text, onclick, tooltip_text, tooltip_widget)
				return btn
			end
			
			self.browse_button = MakeMainMenuButton(_G.STRINGS.ACE.HG, function() if self.found then _G.JoinServer(self.found) end end, _G.STRINGS.UI.MAINSCREEN.TOOLTIP_BROWSE, self.tooltip)
			self.browse_button:Hide()
			self.menu:AddCustomItem(self.browse_button)
		end
	end)
	
	self.upd_task = self.inst:DoPeriodicTask(.1, function()
		local data = TheNet:GetServerListings()
		if #data < 1 or not self.browse_button then
			if not TheNet:IsSearchingServers() then
				TheNet:SearchServers()
			end
			return
		end
		
		for i, server in ipairs(data) do
			if server.tags and server.tags:find("hg_official") then
				self.found = server
				self.browse_button:Show()
				self.upd_task:Cancel()
				self.upd_task = nil
				if TheNet:IsSearchingServers() then
					TheNet:StopSearchingServers()
				end
				break
			end
		end
	end)
	
	local _FadeToScreen, _GoToOnlineScreen = self._FadeToScreen, self._GoToOnlineScreen
	function self:_FadeToScreen(...)
		if TheNet:IsSearchingServers() then
			TheNet:StopSearchingServers()
		end
		return _FadeToScreen(self, ...)
	end
	function self:_GoToOnlineScreen(...)
		if TheNet:IsSearchingServers() then
			TheNet:StopSearchingServers()
		end
		connecting = nil
		found = nil
		return _GoToOnlineScreen(self, ...)
	end
end)
]]